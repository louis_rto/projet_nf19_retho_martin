Voici le git de notre projet de NF19.

Pour pouvoir lancer le wordpress via docker vous devez premierement installer docker sur votre pc en fonction de votre OS.

Pour Windows: https://docs.docker.com/desktop/install/windows-install/
Pour Ubuntu: https://docs.docker.com/engine/install/ubuntu/

Une fois installé vous devez télécharger le dossier git en .zip et le mettre sur votre bureau afin de le déziper. Ensuite il faut se rendre dans le terminal et aller dans le dossier.

Ensuite il suffit d'executer dans le terminal la commande: docker-compose up

Les données wordpress et de la base de données vont se chargés afin de pouvoir afficher le contenu du site wordpress.

Pour aller dans ce site rendez-vous sur votre navigateur web et rentrer: localhost:8000.

Deux scénarios apparaissent, dans un premier cas le site vous connecte sans vous demander de mot de passe, dans ce cas la vous êtes déja sur le site et vous pouvez le voir et le modifier. 
Dans un second cas le site vous demande de vous connecter les identifiants sont:

le login: admin_gift 
le mot de passe: pass_gift

